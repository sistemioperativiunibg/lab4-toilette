package it.unibg.so.lab4;

import java.util.concurrent.Semaphore;

public class Toilette {
	
	private int N; //capacità del bagno
	private int women; /* numero di donne nella toilette*/
	private int men; /* numero di uomini nella toilette*/
	private int waitingWomen; /* numero di donne in attesa di entrare nella toilette*/
	private int waitingMen; /* numero uomini in attesa di entrare nella toilette */

	private Semaphore mutex; /* semaforo per la mutua esclusione */
	private Semaphore womenSem; /*semaforo per la sospensione dei thread donna*/
	private Semaphore menSem; /*semaforo per la sospensione dei thread uomini*/

	public Toilette(int N){ //Inizializzazione
		this.N=N;
		waitingWomen=0;
		waitingMen=0;   
		women=0; 
		men=0;
		mutex = new Semaphore(1);
		womenSem = new Semaphore(0);
		menSem =  new Semaphore(0);
	}

	public void womanIn() throws InterruptedException {   
		mutex.acquire();
		while(women + men >= N || men > 0) {
			waitingWomen++;
			mutex.release();
			womenSem.acquire(); // sospensione donna
			mutex.acquire();
			waitingWomen--;
		}
		women++; 
		System.out.println("Donna "+ Thread.currentThread().getName() + " in bagno....");
		mutex.release();

	}

	public void womanOut() throws InterruptedException {
		mutex.acquire();
		women--;
		if (waitingWomen > 0) //Se ci sono donne che aspettano: risveglio una donna sospesa
			womenSem.release();
		else if(women == 0 && waitingMen > 0) // non ci sono più donne: risveglio tutti gli uomini
			menSem.release(waitingMen);
		
		System.out.println("Donna "+ Thread.currentThread().getName() + " esce dal bagno....");
		mutex.release();
	}

	public void manIn() throws InterruptedException {
		mutex.acquire();
		while (women + men == N || women > 0 || waitingWomen > 0) {
			waitingMen++;
			mutex.release();
			menSem.acquire(); // sospensione uomo
			mutex.acquire();
			waitingMen--;
		}
		men++;   
		System.out.println("Uomo "+ Thread.currentThread().getName() + " in bagno....");
		mutex.release();      
	}

	public void manOut() throws InterruptedException {
		mutex.acquire();
		men--;
		if(waitingWomen > 0 && men == 0) //Se ci sono donne che aspettano e nel bagno non ci sono più uomini, allora risveglio tutte le donne sospese
			womenSem.release(waitingWomen); 
		else if(waitingWomen == 0 && waitingMen > 0) //Se non ci sono donne che aspettano e ci sono, invece, uomini che aspettano, allora risveglio un uomo
			menSem.release();
		
		System.out.println("Uomo "+ Thread.currentThread().getName() + " esce dal bagno....");    
		mutex.release();
	}
}